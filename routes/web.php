<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/', 'PagesController@home');
// The All Route for detail car 
 Route::get('/show/car{id}', 'PagesController@show')->name('show');

 // The All Route for Booking

/* Route::get('/booking', 'BookingController@index')->name('booking');
Route::get('/booking/create', 'BookingController@create')->name('booking.create');
Route::POST('/booking/store/booking', 'BookingController@store')->name('booking.store');
Route::get('/booking/show/booking/{id}', 'BookingController@show')->name('booking.show');
Route::get('/booking/edit/booking/{id}', 'BookingController@edit')->name('booking.edit');
Route::POST('/booking/update/booking/{id}', 'BookingController@update')->name('booking.update');
Route::POST('/booking/delete/booking/{id}', 'BookingController@destroy')->name('booking.delete'); */

Route::resource('booking', 'BookingController');



Auth::routes(['verify' => true]);


Route::get('/profile', 'HomeController@index')->name('profile');


Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');

Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    // The All Route for Cars
    Route::get('/cars', 'CarsController@index')->name('car');
    Route::get('/cars/create', 'CarsController@create')->name('car.create');
    Route::POST('/cars/store/car', 'CarsController@store')->name('car.store');
    Route::get('/cars/show/car/{id}', 'CarsController@show')->name('car.show');
    Route::get('/cars/edit/car/{id}', 'CarsController@edit')->name('car.edit');
    Route::POST('/cars/update/car/{id}', 'CarsController@update')->name('car.update');
    Route::POST('/cars/delete/car/{id}', 'CarsController@destroy')->name('car.delete');

    // The All Route for Cars
    Route::get('/brands', 'BrandsController@index')->name('brand');
    Route::get('/brands/create', 'BrandsController@create')->name('brand.create');
    Route::POST('/brands/store/brand', 'BrandsController@store')->name('brand.store');
    Route::get('/brands/show/brand/{id}', 'BrandsController@show')->name('brand.show');
    Route::get('/brands/edit/brand/{id}', 'BrandsController@edit')->name('brand.edit');
    Route::POST('/brands/update/brand/{id}', 'BrandsController@update')->name('brand.update');
    Route::POST('/brands/delete/brand/{id}', 'BrandsController@destroy')->name('brand.delete');

    // The All Route for Users
    Route::get('/users', 'UsersController@index')->name('brand');
    Route::get('/users/create', 'UsersController@create')->name('user.create');
    Route::POST('/users/store/user', 'UsersController@store')->name('user.store');
    Route::get('/users/show/user/{id}', 'UsersController@show')->name('user.show');
    Route::get('/users/edit/user/{id}', 'UsersController@edit')->name('user.edit');
    Route::POST('/users/update/user/{id}', 'UsersController@update')->name('user.update');
    Route::POST('/users/delete/user/{id}', 'UsersController@destroy')->name('user.delete');


    Route::get('/booking', 'BackBookingController@index')->name('booking');

});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
