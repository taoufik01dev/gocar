@extends('layouts.backend.app')
    @section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Simple Tables
            <small>preview of simple tables</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Simple</li>
          </ol>
        </section>
    
        <section class="content">
            <div class="row">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
    
                <div class="row">
                    {!! Form::model($brand, ['route' => ['brand.update', $brand->id], 'method' => 'POST']) !!}
                    
                
                    <div class="col-md-8">
                        
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', NULL, array('class' => 'form-control' , 'required')) }}
                        </div>
                       
                        
                        
                        
                        
                    </div>
                    <div class="col-md-4">
                        <div class="well">
                            <dl class="dl-horizontal">
                                <dt>Create At:</dt>
                                <dd>{{ date('M j, Y h:ia', strtotime($brand->created_at))}}</dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Update At:</dt>
                                <dd>{{ date('M j, Y h:ia', strtotime($brand->updated_at))}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        {{ Html::linkroute('brand', 'Cancel', NULL, array('class' => 'btn btn-danger')) }}
                    </div>
                
                    <div class="col-md-6">
                        {{ Form::submit('Save Changes', ['class' => 'btn btn-success']) }}
                    </div>
                    
                </div>
                {!! Form::close() !!}
    
            </div>
        </section>
    
        
        </div>
        
@endsection




