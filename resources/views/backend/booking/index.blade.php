@extends('layouts.backend.app')
    @section('content')
            <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Listes Tables
        <small> All Cars </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Cars tables</h3>
    
                  <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
    
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered">
                        <thead>
                            <th>#</th>
                            <th>Date Up</th>
                            <th>Date End </th>
                            <th>Hour Up</th>
                            <th>Hour End</th>
                            <th>User Id</th>
                            <th>create at</th>
                        <th class="text-center" width="130px"><a href="{{ route('booking.create') }}" class="btn btn-primary btn-sm">Create</a></th>
                        </thead>
                        <tbody>
                            <?php $no=1 ?>
                            @foreach ($booking as $key => $value)
                                <tr>
                                    <th>{{ $no++ }}</th>
                                    <td>{{ $value->date_up }}</td>
                                    <td>{{ $value->date_end }}</td>
                                    <td>{{ $value->hour_up }}</td>
                                    <td>{{ $value->hour_end }}</td>
                                    <td>{{ $value->user_id }}</td>
                                    <td>{{ date(' M j Y', strtotime($value->created_at)) }}</td>
                                    
                                    <td>
                                        <a href="{{ route('brand.show', $value->id) }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open "></span></a>
                                        <a href="{{ route('brand.edit', $value->id) }}" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil "></span></a>
                                         {!! Form::open(['method' => 'POST', 'route' => ['brand.delete',$value->id], 'style' => 'display:inline' ]) !!}
                                        {!! Form::button('', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm glyphicon glyphicon-trash ']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
          </div>
    </div>
@endsection



