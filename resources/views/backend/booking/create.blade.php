@extends('layouts.backend.app')
@section('content')
        
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Simple Tables
        <small>preview of simple tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <section class="content">
        <div class="row">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-md-12">
                      {{-- Create Form Brand--}}
               {!! Form::open(['action' => 'BookingController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
               <div class="form-group">
                   {{ Form::label('date_up', 'Date Up') }}
                   {{ Form::text('date_up', NULL, array('class' => 'form-control' , 'required' )) }}
               </div>
              
               {{ Form::submit('Add Booking', array('class' => 'btn btn-success btn-lg btn-block', 'style'=> 'margin-top:20px')) }}


            {!! Form::close() !!}
            </div>

        </div>
    </section>

    
    </div>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>


@endsection















@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Create New Brand</h1>
            </div>
        </div>
        <div class="row">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-12">
              
            </div>
        </div>
    </div>
    


@endsection