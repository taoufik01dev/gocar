@extends('layouts.backend.app')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Listes Tables
        <small> All Cars </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">All Cars</li>
      </ol>
    </section>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Cars tables</h3>
    
                  <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
    
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>ID</th>
                      <th>title</th>
                      <th>Fuel</th>
                      <th>Fiscal Power</th>
                      <th>Number Doors</th>
                      <th>Number Places</th>
                      <th>Gearbox</th>
                      <th>Brand</th>
                      <th>Image</th>
                      <th>Create At</th>
                      <th class="text-center"><a href="{{ route('car.create') }}" class="btn btn-primary btn-sm">Create</a></th>
                    </tr>
                            <?php $no=1 ?>
                            @foreach ($car as $key => $value)
                            
                                <tr>
                                    <th>{{ $no++ }}</th>
                                    <td>{{ $value->title }}</td>
                                    {{-- <td>{{ substr($value->body, 0, 40) }} {{ strlen($value->body) > 40 ? '....' : ""}}</td> --}}
                                    <td>{{ $value->fuel}}</td>
                                    <td>{{ $value->fiscal_power }}</td>
                                    <td>{{ $value->number_doors }}</td>
                                    <td>{{ $value->number_places }}</td>
                                    <td>{{ $value->gearbox }}</td>
                                    <td>{{ $value->brand_id }}</td>
                                    
                                    {{-- <td>
                                    @if($value->brand_id !== null)         
                                      {{  $value->brand_id->name }}
                                      @endif
                                    </td> --}}
                                    {{-- <td > {{dd($value)}}</td> --}}
                                    <td><img style="width:100px" src="{{asset('/cover_image/'.$value->cover_image)}}"></td>
                                    {{-- <td><img style="width:100%" src="{{asset('/storage/app'.$value->cover_image)}}"></td> --}}
                                    <td>{{ date(' M j Y', strtotime($value->created_at)) }}</td>
                                    
                                    <td>
                                        <a href="{{ route('car.show', $value->id) }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open "></span></a>
                                        <a href="{{ route('car.edit', $value->id) }}" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil "></span></a>
                                        {!! Form::open(['method' => 'POST', 'route' => ['car.delete',$value->id], 'style' => 'display:inline' ]) !!}
                                        {!! Form::button('', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm glyphicon glyphicon-trash ']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                        @endforeach
                      
                   
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
          </div>
    </div>
@endsection
