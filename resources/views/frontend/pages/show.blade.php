@extends('layouts.frontend.app')

@section('content')
 <!--== Page Title Area Start ==-->
 <section id="page-title-area" class="section-padding overlay">
    <div class="container">
        <div class="row">
            <!-- Page Title Start -->
            <div class="col-lg-12">
                <div class="section-title  text-center">
                    <h2>Our Cars</h2>
                    <span class="title-line"><i class="fa fa-car"></i></span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <!-- Page Title End -->
        </div>
    </div>
</section>
<!--== Page Title Area End ==-->

    <!--== Car List Area Start ==-->
    <section id="car-list-area" class="section-padding">
            <div class="container">
                <div class="row">
                    <!-- Car List Content Start -->
                    <div class="col-lg-8">
                        <div class="car-details-content">
                                <h2>{{$car->title}} <span class="price">Rent: <b>{{$car->price}}DH</b></span></h2>
                                    <img style="width:100%" src="{{asset('/cover_image/'.$car->cover_image)}}">
                            <div class="car-details-info">
                                <h4>Additional Info</h4>
                                <p>{{$car->body}}</p>
                                <div class="technical-info">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="tech-info-table">
                                                    <table class="table table-bordered">
                                                            <tr>
                                                                <th>Brand</th>
                                                                {{-- <td>@if($car->brand !== null)         
                                                                        {{  $car->brand->name }}
                                                                     @endif
                                                                </td> --}}
                                                            </tr>
                                                            <tr>
                                                                    <th>Year</th>
                                                                    <td>{{$car->year}}</td>
                                                                </tr>
                                                            <tr>
                                                                <th>Color</th>
                                                                <td>{{$car->color}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Number Doors</th>
                                                                <td>{{$car->number_doors}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Number Places</th>
                                                                <td>{{$car->number_places}}</td>
                                                            </tr>
                                                            
                                                        </table>
                                            </div>
                                        </div>
    
                                    </div>
                                </div>
    
                                
                            </div>
                        </div>
                    </div>
                    <!-- Car List Content End -->
    
                    <!-- Sidebar Area Start -->
                    <div class="col-lg-4">
                        <div class="sidebar-content-wrap m-t-50">
                            <!-- Single Sidebar Start -->
                            <div class="single-sidebar">
                                <h3>Add Some Informations</h3>
    
                                <div class="sidebar-body">
                                    {{-- Create Form Booking--}}
                                {!! Form::open(['action' => 'BookingController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                                <div class="form-group">
                                        {!! Form::label('date_up', 'Date Up:') !!}
                                         {!! Form::date('date_up', date('D-m-y'), ['class' => 'form-control']) !!} 
                                </div>
                                <div class="form-group">
                                        {!! Form::label('date_end', 'Date End:') !!}
                                         {!! Form::date('date_end', date('D-m-y'), ['class' => 'form-control']) !!} 
                                </div>

                                <div class="form-group">
                                        {{ Form::label('hour_up', 'Hour Up') }}
                                        {{ Form::select ('hour_up', ['1:00' => '01:00 ',
                                                                           '2:00' => ' 02:00',
                                                                            '3' => ' 03:00',
                                                                            '4' => ' 04:00',
                                                                            '5' => ' 05:00',
                                                                            '6' => ' 06:00',
                                                                            '8' => ' 07:00',
                                                                            '8' => ' 08:00',
                                                                            '9' => ' 09:00',
                                                                            '10' => ' 10:00',
                                                                            '11' => ' 11:00',
                                                                            '12' => ' 12:00',
                                                                            '13' => ' 13:00',
                                                                            '14' => ' 14:00',
                                                                            '15' => ' 15:00',
                                                                            '16' => ' 16:00',
                                                                            '17' => ' 17:00',
                                                                            '18' => ' 18:00',
                                                                            '19' => ' 19:00',
                                                                            '20' => ' 20:00',
                                                                            '21' => ' 21:00',
                                                                            '22' => ' 22:00',
                                                                            '23' => ' 23:00',
                                                                            '24' => ' 24:00',
                                                                        ],
                                                                            1 , ['id' =>'select','class' => 'form-control']) }}
                                    </div>
                                    <div class="form-group">
                                            {{ Form::label('hour_end', 'Hour End') }}
                                            {{ Form::select ('hour_end', ['1' => '01:00 ',
                                                                            '2' => ' 02:00',
                                                                            '3' => ' 03:00',
                                                                            '4' => ' 04:00',
                                                                            '5' => ' 05:00',
                                                                            '6' => ' 06:00',
                                                                            '8' => ' 07:00',
                                                                            '8' => ' 08:00',
                                                                            '9' => ' 09:00',
                                                                            '10' => ' 10:00',
                                                                            '11' => ' 11:00',
                                                                            '12' => ' 12:00',
                                                                            '13' => ' 13:00',
                                                                            '14' => ' 14:00',
                                                                            '15' => ' 15:00',
                                                                            '16' => ' 16:00',
                                                                            '17' => ' 17:00',
                                                                            '18' => ' 18:00',
                                                                            '19' => ' 19:00',
                                                                            '20' => ' 20:00',
                                                                            '21' => ' 21:00',
                                                                            '22' => ' 22:00',
                                                                            '23' => ' 23:00',
                                                                            '24' => ' 24:00',
                                                                            ],
                                                                                1 , ['id' =>'select','class' => 'form-control']) }}
                                        </div>
                               
                                
                                {{ Form::submit('Add Booking', array('class' => 'btn btn-success btn-lg btn-block', 'style'=> 'margin-top:20px')) }}
                            {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- Single Sidebar End -->
    
                            
                        </div>
                    </div>
                    <!-- Sidebar Area End -->
                </div>
            </div>
        </section>
        <!--== Car List Area End ==-->
    
@endsection