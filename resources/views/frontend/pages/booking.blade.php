@extends('layouts.frontend.app')

@section('content')
 <!--== Page Title Area Start ==-->
 <section id="page-title-area" class="section-padding overlay">
    <div class="container">
        <div class="row">
            <!-- Page Title Start -->
            <div class="col-lg-12">
                <div class="section-title  text-center">
                    <h2>Our Cars</h2>
                    <span class="title-line"><i class="fa fa-car"></i></span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <!-- Page Title End -->
        </div>
    </div>
</section>
<!--== Page Title Area End ==-->

<!--== Car List Area Start ==-->
<section id="car-list-area" class="section-padding">
    <div class="container">
        <div class="row">
            <!-- Car List Content Start -->
         
            <!-- Car List Content End -->
            
            
            <!-- Sidebar Area Start -->
            <div class="col-lg-5">
                <div class="sidebar-content-wrap m-t-50">
                    <!-- Single Sidebar Start -->
                    <div class="single-sidebar">
                        <h3>Booking Car Now</h3>

                        <div class="sidebar-body">
                            {{-- Create Form Booking--}}
                            {!! Form::open(['action' => 'BookingController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                            
                            <div class="form-group">
                                {{ Form::label('date_up ', 'date_up') }}
                                {{ Form::text('date_up ', NULL, array('class' => 'form-control' , 'required' )) }}
                            </div>
                             <div class="form-group">
                                {{ Form::label('date_end ', 'date_end') }}
                                {{ Form::text('date_end', NULL, array('class' => 'form-control' , 'required' )) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('hour_up ', 'hour_up') }}
                                {{ Form::text('hour_up ', NULL, array('class' => 'form-control' , 'required' )) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('hour_end ', 'hour_end') }}
                                {{ Form::text('hour_end ', NULL, array('class' => 'form-control' , 'required' )) }}
                            </div>
                            
                            {{ Form::submit('Booking Car', array('class' => 'btn btn-primary ', 'style'=> 'margin-top:20px')) }}


                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- Single Sidebar End -->

                </div>
            </div>
            <!-- Sidebar Area End -->
        </div>
    </div>
</section>
<!--== Car List Area End ==-->
@endsection