<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\car;
use App\brand;
use Auth;

use Storage;

use Image;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function __construct()
    {
        $this->middleware('auth:admin');
    } */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $car = Car::all();
        /* dd($car);
        exit; */
        return view('backend.cars.index',compact('car'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $car = Car::all();
        $brands =  Brand::pluck('name', 'id');
        
        return view('backend.cars.create', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        {
            $car = new Car;
    
          $car->title = $request->title;
           $car->body = $request->body;
           $car->price = $request->price;
           $car->fuel = $request->fuel;
           $car->color = $request->color;
           $car->year = $request->year;
           $car->fiscal_power = $request->fiscal_power;
           $car->number_doors = $request->number_doors;
           $car->number_places = $request->number_places;
           $car->gearbox = $request->gearbox;
           $car->cover_image = $request->cover_image;
           $car->brand_id = $request->brand_id;
    
           $this->validate($request, array(
               
               'title' => 'required|max:255',
               'body'=> 'required',
               'price'=> 'required',
               'fuel'=> 'required',
               'year'=> 'required',
               'color'=> 'required',
               'fiscal_power'=> 'required',
               'number_doors'=> 'required',
               'number_places'=> 'required',
               'gearbox'=> 'required',
               'brand_id'=> 'required',
               'cover_image' => 'image|nullable|max:1999'
           ));
    
           // Handle File Upload
       /* if($request->hasFile('cover_image')) {
        $car->cover_image = $request->cover_image->store('cover_image');
    } */
    $file = $request->file('cover_image');
            $name = time().'_'.$file->getClientOriginalName();
            $file->move(public_path().'/cover_image/',$name);
            
            $car->cover_image = $name;
           $car->save();
    
           return redirect()->route('car');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = Car::find($id);
        return view('backend.cars.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::find($id);
        $brands =  Brand::pluck('name', 'id');
        return view('backend/cars/edit', compact('car','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $car = Car::find($id);

       $car->title = $request->title;
       $car->body = $request->body;
       $car->price = $request->price;
       $car->fuel = $request->fuel;
       $car->color = $request->color;
       $car->year = $request->year;
       $car->fiscal_power = $request->fiscal_power;
       $car->number_doors = $request->number_doors;
       $car->number_places = $request->number_places;
       $car->gearbox = $request->gearbox;
       $car->brand_id = $request->brand_id;

        $this->validate($request, array(
            'title' => 'required|max:255',
            'body'=> 'required',
            'price'=> 'required',
            'fuel'=> 'required',
            'year'=> 'required',
            'color'=> 'required',
            'fiscal_power'=> 'required',
            'number_doors'=> 'required',
            'number_places'=> 'required',
            'gearbox'=> 'required',
            'brand_id'=> 'required',
            
        ));
        

        $car->save();

        return redirect()->route('car');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::find($id);
        $car->delete();
        return redirect()->route('car');
    }
}
