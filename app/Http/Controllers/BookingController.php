<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\booking;
use Auth;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     */

    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $booking = Booking::where('user_id', Auth::user()->get());
        return view('backend\booking\index', compact('booking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $booking = Booking::all();
        return view('\frontend\pages\create', compact('booking.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'date_up' => 'required',
            'date_end' => 'required',
            'hour_up' => 'required',
            'hour_end' => 'required',
        ));

        $booking = new Booking;
        $booking->date_up = $request->date_up;
        $booking->date_end = $request->date_end;
        $booking->hour_up = $request->hour_up;
        $booking->hour_end = $request->hour_end;

        $booking->user_id = Auth::user()->id;

        $booking->save();

       return redirect()->route('profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        $booking->delete();
        return redirect()->route('booking');
    }
}
