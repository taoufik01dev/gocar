<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\car;
use App\brand;

class PagesController extends Controller
{
    /* public function __construct()
    {
        $this->middleware('auth:web');
    } */
    public function home(Request $request)
    {
        $qBrand = $request->get('q_brand');
        if ($qBrand && $qBrand != '') {
            $car = Car::where('brand_id', $qBrand)
            //->orderBy('name', 'desc')
            //->take(10)
            ->get();
        } else {
            $car = Car::orderBy('id','desc')->paginate(6);
        }
        //$brands =  Brand::pluck('name', 'id')->prepend('selectioner'); 
        $brands =  Brand::all(); 


        /* dd($car);
        exit; */
        return view('frontend.pages.home',compact('car','brands', 'qBrand'));
    }


    public function show($id)
    {
        $car = Car::find($id);
        return view('frontend/pages/show', compact('car'));
    }
}
