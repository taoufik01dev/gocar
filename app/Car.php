<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use SoftDeletes;
    // Table Name
    protected $table = 'cars';
    protected $dates = ['deleted_at'];
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    public function brand() 
    {
        return $this->hasMany(Brand::class);
    }

    public function booking() 
    {
        return $this->hasMany(Booking::class);
    }
}
